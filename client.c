
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define MSG_SIZE 20

const char charset[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

void rand_str( char * dest, unsigned short length);

void err_sigpipe(int sig);



int main(int argc, char **argv)
{
    int msgN = -1, i = 0;
    unsigned short port = 0;    
    char * msg = NULL ;
    char * ans = NULL;
    struct hostent * serv;
    int sockfd = 0 ;
    struct sockaddr_in serv_addr;
    
    int err = 0, nsent = 0 , len = 0;
    
    
    srand(time(NULL));
    
    if(argc < 4)
     {
        printf( "Wrong amount of arguments. Use the programm like that: %s <msg-number> <hostname> <port>.\n", *argv );
        exit(1);     
     }
     
    
    msg = (char*)malloc(MSG_SIZE + 1);
    ans = (char*)malloc(MSG_SIZE + 1);
    
    if((msg == NULL) || (ans == NULL))
     {
        perror("Wrong memory allocation.\n");
        exit(1);
     }
    
    sscanf(*(argv + 1),"%d", &msgN);
    sscanf(*(argv + 3),"%hu", &port);
        
    if( (msgN < 0) || (port < 0) )
     {
        fprintf(stderr,"Wrong arguments.\n");
        exit(1);
     }
    
    serv = gethostbyname(argv[2]);
    
    if(serv == NULL)     {
     	perror("No such host availible.\n");
     	exit(1);     
     }
    
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    memcpy((char *)&serv_addr.sin_addr.s_addr, (char *)serv->h_addr, serv->h_length);
     
    if( (sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0 ) 
     {
        perror("Failed to open a socket.\n");
        exit(1);     
     }
    
    if ( connect(sockfd, (struct sockaddr * )&serv_addr, sizeof(serv_addr) ) < 0)
     {
        perror("Failed to connect to the socket.\n");
        exit(1);    
     }
    
    signal(SIGPIPE,err_sigpipe);
    
    for(i = 0; i < msgN; ++i)
     {
        rand_str(msg, MSG_SIZE);
        len = 0;
        do
        {
        	if( (nsent = send(sockfd, (void*)(msg + len), strlen(msg) - len,0)) < 0)
         	 {
            	perror("Error occured while sending a message.\n");
            	exit(1);         
         	 }
         	len += nsent;
        }while( len < strlen(msg));	 
         
        if( ( (err = recv(sockfd, (void*)ans, MSG_SIZE + 1, 0) ) < 0 ) )
         {
            perror("Error occured while recieving a message.\n");
            exit(1);         
         }
        else if( err == 0 )
         {
         	printf("The server has closed the connection.\n");
         	break;
         }
        else 
         {
            if(!strcmp(ans,"OK") )
             { 
             }
            else if ( !strcmp(ans,"FAIL") )
             {
             	fprintf(stderr,"Answer for the message is FAIL. Halting now.\n");
             	exit(1);             
             }
            else
             {
                fprintf(stderr,"Unexpected answer of the server.\n");
                exit(1);
             }  
         }  
      sleep(1);  
     } 
       
    if ( msg != NULL )
      free(msg);
    if ( ans != NULL )
      free(ans);
      
 return 0;
}



void rand_str(char * dest, unsigned short length) {

    while (length-- > 0) {
        size_t index = (double) rand() / RAND_MAX * (sizeof(charset) - 1);
        *dest++ = charset[index];
    }
    *dest = '\0';
}


void err_sigpipe(int sig){
    
    printf("Socket is unavailible for work.\n");
    exit(1);
}

