#!/usr/bin/env python

from os import fork, waitpid, WNOHANG,kill
import sys
from time import localtime, strftime
import signal
import socket
import logging
import pymongo


def sig_killed(signum, frame):
    for pid in children:
      kill(pid, signal.SIGTERM)  
    exit("Programm is killed\n")


def sig_child(signum, frame):
    waitpid(-1,WNOHANG)
    return


def work_with_sock(sock, collection, logger):  

    def child_sig_killed(signum, frame):
        sock.send("FAIL")
        sock.close()
        exit(0)

    signal.signal(signal.SIGTERM, child_sig_killed)
    try: 
        ip, p = sock.getpeername()   

        msg = sock.recv(21)
        while( msg != '' ):
            time = strftime("%m/%d/%Y %I:%M:%S %p", localtime())
       
            logger.info("%s %s %s %s", time, ip, p, msg)        
            try:
                collection.insert({'msg': msg , 'time': time, 'ip': ip, 'port': p })
            except pymongo.errors.OperationFailure as err:
                sock.send("FAIL")
                exit(err)
            else:
                sock.send("OK")
            msg = sock.recv(21)
        else:
            logger.info("The work is done.\n") 
                   
    except KeyboardInterrupt:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()                   
    exit(0)


def main():

    signal.signal(signal.SIGTERM, sig_killed)
    signal.signal(signal.SIGCHLD, sig_child)

    if (len(sys.argv) < 4):
        exit("Call this programm exactly with 3 arguments: <port> <mongodb://host:port/> <db-name>\n")
   
    try:
        port = int(sys.argv[1],10)
    except ValueError as t:
        exit(t)    

    start_sock = None
    sock = None
    sock_addr = 0

    try:
        mongo_client = pymongo.MongoClient(sys.argv[2])
    except pymongo.errors.PyMongoError:
        exit("Can't gain access to the mongodb")

    database = mongo_client[sys.argv[3]]
    collection = database["messages"]
    
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter("%(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    
    
    try:
    
        start_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)

        start_sock.bind(('', port))
        start_sock.listen(5) 
    
        while( True ):  
            try:
            
                try:
                    sock,sock_addr = start_sock.accept()
                except socket.error:
                    continue
                    
                pid = fork()
                if( pid == 0):
                    start_sock.close()
                    work_with_sock(sock, collection, logger)
                else:
                    sock.close()
                    children.append(pid)    
                
            except socket.timeout as t:
                break
            
    except socket.error as msg:
        exit('Error occured while working with a socket: %s.\n'%msg)
    except KeyboardInterrupt:
        if start_sock:
            start_sock.shutdown(socket.SHUT_RDWR)
            start_sock.close()       
        exit("Programm is being interrupted\n")  

    if start_sock:
        start_sock.shutdown(socket.SHUT_RDWR)
        start_sock.close()


if (__name__ == "__main__"):
    children = []
    main() 



